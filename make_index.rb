html = %q(
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>ISBN scraper</title>
  </head>
  <body>
    <h1>ISBN scraper</h1>
    <ol>
      $items
    </ol>
  </body>
</html>
)

files = Dir.glob('./data/*').sort
items = files.map{ |e| '<li><a href="' + e + '">' + File.basename(e) + '</a></li>'}

f = File.open('index.html', 'w')
f.puts html.gsub('$items', items.join("\n"))
f.close
